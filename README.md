Here you will my submitted code for the Man Group Dataset Challenge.

<b>Task description</b> (straight from the event page):<br/>
Introducing the Algorithmic Trading Society’s and Man Group’s inaugural joint event: US Stock ETFs Analysis Challenge. 

The challenge is a great way to enter the world of quantitative analysis and algorithmic trading. On the 18th of October you will be able to present your analysis to Man Group employees - we are looking forward to seeing what you discover!

Data: 
Huge Stock Market Dataset from Kaggle
https://www.kaggle.com/borismarjanovic/price-volume-data-for-all-us-stocks-etfs/home

Task:
You can choose one of the two tasks below:
1. Create a trading strategy with a backtest
2. Find a unique insight or feature of the market (this dataset only, it can be anything that you have discovered)

Rules:
• You can only use Python (2 or 3) with these libraries:
NumPy, SciPy, Pandas, some static plotting libraries e.g. matplotlib, seaborn
No Statsmodel/skLearn or any ML frameworks/packages
• Teams of 1-4
• No additional data input allowed







Link to the event page:
https://en-gb.facebook.com/events/264624620852288/
Last accessed: 17/02/2019