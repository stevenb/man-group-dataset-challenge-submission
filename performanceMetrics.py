##### performance metrics #####
###############################
import numpy as np
import pandas as pd


def pnlsFromReturns(returns): # pnls = regular profit & loss numbers???
    # Input returns must be a pd.Series object
    length = len(returns)
    pnls = np.zeros(length)
    pnls[0] = 1.0 + returns[0]
    for i in range(1,length):
        pnls[i] = pnls[i-1] * (1.0 + returns[i])
    pnls = pd.Series(pnls, index = returns.index)
    return pnls


def drawdownsFromPnls(pnls):
    length = len(pnls)
    historicHigh = pnls[0]
    drawdowns = np.zeros(length)
    for i in range(1,length):
        if pnls[i] > historicHigh:
            historicHigh = pnls[i]
        drawdowns[i] = 1.0 - pnls[i]/historicHigh
    drawdowns = pd.Series(drawdowns, index = pnls.index)
    return drawdowns


def get_compound_interest(p_opening_balance, interest, no_periods):
    """
    Parameters
    ----------
    p_opening_balance: `float`
    interest:          `float`
        Interest rate in decimals
    no_periods:        `int`

    Returns
    -------
    `float`

    """
    return p_opening_balance*((1+interest)**no_periods -1)


def get_annual_return(returns, yearly_trading_days=261.):
    """
    Parameters
    ----------
    returns: `numpy.ndarray`
        returns yielded on historical data during backtesting
    yearly_trading_days: `float`

    Returns
    -------
    `float`
        see function name
    """
    return np.mean(returns)*yearly_trading_days


def get_annual_volatility(returns, yearly_trading_days=261.):
    """
    Parameters
    ----------
    returns: `numpy.ndarray`
        returns yielded on historical data during backtesting
    yearly_trading_days: `float`

    Returns
    -------
    `float`
        see function name
    """
    return np.std(returns)*np.sqrt(yearly_trading_days)


def get_sharpe_ratio(returns):
    """
    The Sharpe ratio is the average return earned in excess of the risk-free rate per unit of volatility
    or total risk. Subtracting the risk-free rate from the mean return, the performance associated with
    risk-taking activities can be isolated. One intuition of this calculation is that a portfolio engaging
    in “zero risk” investment, such as the purchase of U.S. Treasury bills (for which the expected return
    is the risk-free rate), has a Sharpe ratio of exactly zero. Generally, the greater the value of the
    Sharpe ratio, the more attractive the risk-adjusted return.
    sharpe_ratio >= 1 is good, 2 better, 3 very good

    Definition:
    Sharpe ratio = (Mean portfolio return − Risk-free rate)/Standard deviation of portfolio return

    Alternatives:
    It can be inaccurate when applied to portfolios or assets that do not have a normal distribution of
    expected returns. Many assets have a high degree of kurtosis ('fat tails') or negative skewness.
    The Sharpe ratio also tends to fail when analyzing portfolios with significant non-linear risks, such
    as options or warrants. Alternative risk-adjusted return methodologies have emerged over the years,
    including the Sortino Ratio, Return Over Maximum Drawdown (RoMaD), and the Treynor Ratio.

    Remark:
    Modern Portfolio Theory states that adding assets to a diversified portfolio that have correlations of
    less than 1 with each other can decrease portfolio risk without sacrificing return. Such diversification
    will serve to increase the Sharpe ratio of a portfolio.

    Remark:
    Risk free return is set to zero!
    """
    return get_annual_return(returns)/get_annual_volatility(returns)


def get_downside_deviation(returns, mean_return):
    """
    Parameters
    ----------
    returns: `numpy.ndarray`
        returns yielded on historical data during backtesting
    mean_return: `numpy.float64`
        mean return of returns

    Returns
    -------
    `numpy.float64`
        in contrast to std only the negative deviation gets recorded since the positive side has no negative
        impact.
        Note: the unbiased variance estimator is being used
    """
    diff_sum = 0
    for return_ in returns:
        diff = min(return_ - mean_return, 0)
        diff_sum += diff*diff

    return np.sqrt(1./(len(returns)-1.)*diff_sum)


def get_sortino_ratio(returns, yearly_trading_days=261.):
    """
    Parameters
    ----------
    mean_return:               `numpy.float64`
        mean return of returns
    risk_free_rate:

    downside_deviation_return: `numpy.float64`
        in contrast to std only the negative deviation gets recorded since the positive side has no negative
        impact.

    Returns
    -------
    `numpy.float64`
        in contrast to std only the negative deviation gets recorded since the positive side has no negative
        impact.
        Note: the unbiased variance estimator is being used
    """
    annual_return = get_annual_return(returns)
    downside_dev = get_downside_deviation(returns, np.mean(returns))
    return annual_return/(downside_dev*np.sqrt(yearly_trading_days))


def get_max_drawdown(returns):
    """
    Parameters
    ----------
    returns: `numpy.ndarray`
        returns yielded on historical data during backtesting

    Returns
    -------
    `float`
        see function name
    """
    pnls = pnlsFromReturns(returns)
    drawdowns = drawdownsFromPnls(pnls)
    return max(drawdowns)


def Return_Over_Maximum_Drawdown(returns, max_drawdown):
    """
    Parameters
    ----------
    mean_return: `numpy.float64`
        mean return of returns
    max_drawdown: `numpy.float64`

    Returns
    -------
    `numpy.float64`
        dito
    """
    return get_annual_return(returns)/max_drawdown


def get_accumulated_return(returns):
    """
    Parameters
    ----------
    returns: `numpy.ndarray`
        returns yielded on historical data during backtesting

    Returns
    -------
    `numpy.float64`
        dito
    """
    return np.sum(returns)


def get_mean_return(returns):
    """
    Parameters
    ----------
    returns: `numpy.ndarray`
        returns yielded on historical data during backtesting

    Returns
    -------
    `numpy.float64`
        dito
    """
    return np.mean(returns)


def get_std_return(returns):
    """
    Parameters
    ----------
    returns: `numpy.ndarray`
        returns yielded on historical data during backtesting

    Returns
    -------
    `numpy.float64`
        dito
    """
    return np.std(returns)


def print_performance_metrics(returns):
    print('\nPerformance Metrics:')
    print('annual return: %s' % get_annual_return(returns))
    print('annual volatility: %s' % get_annual_volatility(returns))
    print('sharpe ratio: %s' % get_sharpe_ratio(returns))
    print('sortino ratio: %s' % get_sortino_ratio(returns))
    print('max drawdown: %s' % get_max_drawdown(returns))
    print('return over maximum drawdown: %s' % Return_Over_Maximum_Drawdown(returns, get_max_drawdown(returns)))
