#!/Users/battilanast/anaconda3/bin/python3

import numpy as np
import pandas as pd

import performanceMetrics as metrics



# import csv data
sp500_data = pd.read_csv("/Users/battilanast/Dropbox/My_Stuff/Marlon_Venture/Man_Group_Dataset_Challenge/Data/ETFs/spy.us.txt", index_col=0)


# run ma crossover
def ma_crossover(data, fw, sw, commission_rate, column='Close'):
    """
    Parameters
    ----------
    data: `pandas.DataFrame`
        historical data
    fw: `int`
        fast window
    sw: `int`
        slow window
    commission_rate: `float`

    Returns
    -------

    """
    fastMA = data.rolling(fw).mean()
    slowMA = data.rolling(sw).mean()
    # deprecated:
    #fastMA = pd.rolling_mean(data, fw)     # rolling mean = moving average
    #slowMA = pd.rolling_mean(data, sw)

    previous_fastMA = fastMA.shift(1)
    previous_slowMA = slowMA.shift(1)
    crossing_bull = (fastMA >= slowMA) & (previous_fastMA <= previous_slowMA)
    crossing_bear = (fastMA <= slowMA) & (previous_fastMA >= previous_slowMA)
    crossing_bull_dates = slowMA.loc[crossing_bull[column].values]
    crossing_bear_dates = slowMA.loc[crossing_bear[column].values]
    bear_bull_signals = [crossing_bear_dates, crossing_bull_dates]

    signal = pd.DataFrame(index=crossing_bull.index, columns=['signal'])
    signal[crossing_bull[column].values] = 1
    signal[crossing_bear[column].values] = -1
    signal.fillna(method='ffill', inplace=True)
    signal.fillna(0, inplace=True)

    return signal, bear_bull_signals, fastMA, slowMA

signals, bear_bull_signals, fastMA, slowMA = ma_crossover(sp500_data, 200, 1, 0)

# run backtesting
def backtest(data, commission_rate, trading_signals):
    """
    Parameters
    ----------
    data: `pandas.DataFrame`
        historical data
    commission_rate: `float`
    trading_signals: `pandas.DataFrame`

    Returns
    -------

    """
    my_return = 0.
    long = False
    first = True
    old_price = 0.
    total_commission_payed = 0.
    recorded_returns = []
    new_dates = []
    new_returns = []
    for date, row in trading_signals.iterrows():
        # do not trade first bit since we need some burn in time
        """
        if first and row['signal'] != 0:
            first = False
            buy_in_price = data.prices[date]
            my_return = my_return - commission_rate
            total_commission_payed += commission_rate
            if row['signal'] == 1:
                long = True
            # is already default
            # else:
            #     long = False
        """

        if not long and row['signal']==1:
            new_price = data.xs(date).iat[0] # data[date]
            new_returns.append(new_price/old_price - 1. if old_price > 0. else 0.)
            new_dates.append(date)
            price_diff = abs(old_price - float(new_price))
            old_price = new_price
            my_return += price_diff - commission_rate
            recorded_returns.append(price_diff - commission_rate)
            total_commission_payed += commission_rate
            long = True

        if long and row['signal']==-1:
            new_price = data.xs(date).iat[0] # data[date]
            new_returns.append(new_price/old_price - 1. if old_price > 0. else 0.)
            new_dates.append(date)
            price_diff = abs(old_price - new_price)
            old_price = new_price
            my_return += price_diff - commission_rate
            recorded_returns.append(price_diff - commission_rate)
            total_commission_payed += commission_rate
            long = False

        new_series = pd.Series(new_returns, index=new_dates, name='return')

    return new_series

returns = backtest(sp500_data, 0, signals)


# run performance metrics
metrics.print_performance_metrics(returns)
